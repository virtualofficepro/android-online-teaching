# Online Teaching Platform 

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### Tech

* Android

### Installation 

Clone this repository and import into Android Studio

```
git clone https://biplabchandranag@bitbucket.org/biplabchandranag/online-teaching.git
```

### License 

MIT
