package com.online.teaching

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.online.teaching.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun setContentViewId(): Int {
        return R.layout.activity_main
    }

    override fun configureView() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initNavigation()
    }

    private fun initNavigation() {
        val hostFragment = (supportFragmentManager
                .findFragmentById(R.id.hostFragment) as NavHostFragment?)!!
        NavigationUI.setupWithNavController(navView!!, hostFragment.navController)
    }
}