package com.online.teaching.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.online.teaching.R
import com.online.teaching.base.BaseFragment

class HomeFragment : BaseFragment() {
    var refresh: SwipeRefreshLayout? = null
    var rvArticle: RecyclerView? = null
    var rvCourse: RecyclerView? = null
    var rvCategory: RecyclerView? = null

    override fun setContentViewId(): Int {
        return R.layout.fragment_home
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        refresh!!.setOnRefreshListener(refreshListener)
        initCourse()
        initCategory()
        initArticle()
        return rootView
    }

    private val refreshListener = OnRefreshListener {
        showDefaultToast("test refresh")
        refresh!!.isRefreshing = false
    }

    private fun initCourse() {
        val adapter = CourseAdapter(requireContext())
        val llm = LinearLayoutManager(getContext())
        llm.orientation = RecyclerView.HORIZONTAL
        rvCourse!!.layoutManager = llm
        rvCourse!!.adapter = adapter
    }

    private fun initCategory() {
        val adapter = CategoryAdapter(requireContext())
        val llm = LinearLayoutManager(getContext())
        llm.orientation = RecyclerView.HORIZONTAL
        rvCategory!!.layoutManager = llm
        rvCategory!!.adapter = adapter
    }

    private fun initArticle() {
        val adapter = ArticleAdapter(requireContext())
        val llm = LinearLayoutManager(getContext())
        llm.orientation = RecyclerView.HORIZONTAL
        rvArticle!!.layoutManager = llm
        rvArticle!!.adapter = adapter
    }
}