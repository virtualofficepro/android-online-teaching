package com.online.teaching.ui.login

import android.content.Intent
import com.online.teaching.MainActivity
import com.online.teaching.R
import com.online.teaching.base.BaseActivity
import com.online.teaching.ui.signup.SignupActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {
    override fun setContentViewId(): Int {
        return R.layout.activity_login
    }

    override fun configureView() {
        onclick()
    }

    private fun onclick() {
        signIn.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
        signUp.setOnClickListener{
            startActivity(Intent(this,SignupActivity::class.java))
        }
        back.setOnClickListener{
            showErrorToast("back")
        }
    }
}