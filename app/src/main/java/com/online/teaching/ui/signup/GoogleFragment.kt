package com.online.teaching.ui.signup

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.online.teaching.R
import com.online.teaching.base.BaseFragment
import com.online.teaching.ui.login.LoginActivity

class GoogleFragment : BaseFragment() {
    override fun setContentViewId(): Int {
        return R.layout.fragment_google
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return rootView
    }

    fun onclick(view: View) {
        when (view.id) {
            R.id.google -> loadRegisterFragment()
            R.id.signIn -> {
                startActivity(Intent(context, LoginActivity::class.java))
                showErrorToast("back")
            }
            R.id.back -> showErrorToast("back")
        }
    }

    private fun loadRegisterFragment() {
        val fragment = RegisterFragment()
        assert(fragmentManager != null)
        val transaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}