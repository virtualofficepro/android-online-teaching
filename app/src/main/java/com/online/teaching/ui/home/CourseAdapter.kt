package com.online.teaching.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.online.teaching.R
import com.online.teaching.utils.Utils
import de.hdodenhof.circleimageview.CircleImageView

class CourseAdapter(private val context: Context) : RecyclerView.Adapter<CourseAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val v = inflater.inflate(R.layout.row_course, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Utils.loadImageView(R.drawable.img_placeholder, holder.image)
        Utils.loadImageView(R.drawable.img_profile, holder.profile)
        holder.title!!.text = "Dummy course title"
        holder.price!!.text = "tk 5000"
        holder.name!!.text = "Teacher Name"
        holder.member!!.text = "10"
        holder.rating!!.rating = 3f
    }

    override fun getItemCount(): Int {
        return 10
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView? = null
        var profile: CircleImageView? = null
        var title: TextView? = null
        var price: TextView? = null
        var name: TextView? = null
        var member: TextView? = null
        var rating: RatingBar? = null
    }
}