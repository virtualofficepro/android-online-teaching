package com.online.teaching.ui.signup

import android.os.Bundle
import android.view.View
import com.online.teaching.R
import com.online.teaching.base.BaseActivity

class SignupActivity : BaseActivity() {
    override fun setContentViewId(): Int {
        return R.layout.activity_signup
    }

    override fun configureView() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showFragment(savedInstanceState)
    }

    private fun showFragment(savedInstanceState: Bundle?) {
        if (findViewById<View?>(R.id.container) != null) {
            if (savedInstanceState != null) {
                return
            }
            val googleFragment = GoogleFragment()
            googleFragment.arguments = intent.extras
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, googleFragment).commit()
        }
    }
}