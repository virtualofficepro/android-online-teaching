package com.online.teaching.ui.course

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.online.teaching.R
import com.online.teaching.base.BaseFragment

class CourseFragment : BaseFragment() {
    override fun setContentViewId(): Int {
        return R.layout.fragment_course
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return rootView
    }
}