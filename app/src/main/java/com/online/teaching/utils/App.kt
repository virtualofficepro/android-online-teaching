package com.online.teaching.utils

import android.app.Application
import android.content.Context
import timber.log.Timber
import timber.log.Timber.DebugTree
import java.lang.ref.WeakReference

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
        mContext = WeakReference(applicationContext)
    }

    companion object {
        private var mContext: WeakReference<Context>? = null
        val context: Context?
            get() = mContext!!.get()
    }
}