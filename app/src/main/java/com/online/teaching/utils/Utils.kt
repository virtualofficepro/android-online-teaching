package com.online.teaching.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.online.teaching.R
import kotlin.coroutines.coroutineContext

object Utils {
    fun loadImageView(imgId: Int, imageView: ImageView?) {
        Glide.with(App.context!!)
                .load(imgId)
                .into(imageView!!)
    }

    fun loadImageView(url: String, placeHolderImage: Int, imageView: ImageView?) {
        Glide.with(App.context!!)
                .load(url)
                .placeholder(placeHolderImage)
                .error(R.drawable.img_placeholder)
                .into(imageView!!)
    }
}