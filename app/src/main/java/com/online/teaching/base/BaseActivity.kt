package com.online.teaching.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pranavpandey.android.dynamic.toasts.DynamicToast

abstract class BaseActivity : AppCompatActivity() {

    abstract fun setContentViewId(): Int

    abstract fun configureView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(setContentViewId())
        configureView()
    }

    protected fun showDefaultToast(message: String?) {
        DynamicToast.make(this, message).show()
    }

    protected fun showErrorToast(message: String?) {
        DynamicToast.makeError(this, message).show()
    }

    protected fun showSuccessToast(message: String?) {
        DynamicToast.makeSuccess(this, message).show()
    }

    protected fun showWarningToast(message: String?) {
        DynamicToast.makeWarning(this, message).show()
    }
}