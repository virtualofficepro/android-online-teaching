package com.online.teaching.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.pranavpandey.android.dynamic.toasts.DynamicToast

abstract class BaseFragment : Fragment() {

    protected var rootView: View? = null
    abstract fun setContentViewId(): Int

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(setContentViewId(), container, false)
        return rootView
    }

    protected fun showDefaultToast(message: String?) {
        DynamicToast.make(requireContext(), message).show()
    }

    protected fun showErrorToast(message: String?) {
        DynamicToast.makeError(requireContext(), message).show()
    }

    protected fun showSuccessToast(message: String?) {
        DynamicToast.makeSuccess(requireContext(), message).show()
    }

    protected fun showWarningToast(message: String?) {
        DynamicToast.makeWarning(requireContext(), message).show()
    }
}